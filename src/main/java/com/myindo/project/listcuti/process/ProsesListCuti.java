/**
 * 
 */
package com.myindo.project.listcuti.process;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.listcuti.dao.GetListCuti;
import com.myindo.project.listcuti.model.HasilGet;
import com.myindo.project.listcuti.model.RequestModel;
import com.myindo.project.listcuti.model.ResponseContentModel;
import com.myindo.project.listcuti.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesListCuti implements Processor{
	Logger log = Logger.getLogger("ProsesListCuti");
	ObjectMapper mapper = new ObjectMapper();
	
	GetListCuti cuti = new GetListCuti();
	String codeResponse;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		
		RequestModel requestModel = gson.fromJson(body, RequestModel.class);
		
		ArrayList<HasilGet> hasilGets = cuti.listCuti(requestModel);
		
		if (hasilGets.isEmpty()) {
			codeResponse ="1111";
			log.info("Get data gagal");
		} else {
			codeResponse ="0000";
			log.info("Get data berhasil");
		}
		
		String res = response(codeResponse, hasilGets);
		Response response = Response.status(HttpStatus.OK_200).entity(res)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		
		exchange.getOut().setBody(response);
	}

	private String response(String codeResponse, ArrayList<HasilGet> hasilGets) {
		// TODO Auto-generated method stub
		String response = "";
		
		try {
			ResponseModel responseModel = new ResponseModel();
			ArrayList<ResponseContentModel> contentModel = new ArrayList<>();
			
			if (codeResponse.equals("0000")) {
				for (int i = 0; i < hasilGets.size(); i++) {
					ResponseContentModel rContentModel = new ResponseContentModel();
					responseModel.setResponseMessage("Success");
					rContentModel.setId_pengajuan(hasilGets.get(i).getId_pengajuan());
					rContentModel.setNpk(hasilGets.get(i).getNpk());
					rContentModel.setTgl_cuti(hasilGets.get(i).getTgl_cuti());
					rContentModel.setTgl_masuk(hasilGets.get(i).getTgl_masuk());
					rContentModel.setJml_hari(hasilGets.get(i).getJml_hari());
					rContentModel.setKeterangan(hasilGets.get(i).getKeterangan());

					contentModel.add(rContentModel);
				}
			} else {
				ResponseContentModel model = new ResponseContentModel();
				responseModel.setResponseMessage("Failed");

				contentModel.add(model);
			}
			
			GregorianCalendar gCalendar = new GregorianCalendar();

			int year = gCalendar.get(Calendar.YEAR);
			int month = gCalendar.get(Calendar.MONTH) + 1;
			int day = gCalendar.get(Calendar.DAY_OF_MONTH);
			int hour = gCalendar.get(Calendar.HOUR_OF_DAY);
			int minute = gCalendar.get(Calendar.MINUTE);
			int second = gCalendar.get(Calendar.SECOND);

			responseModel.setResponseCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + second).toString());
			responseModel.setContent(contentModel);
			
			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
			log.info("Gagal");
		}
		return response;
	}
}
