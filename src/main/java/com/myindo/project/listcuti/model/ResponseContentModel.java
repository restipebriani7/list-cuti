/**
 * 
 */
package com.myindo.project.listcuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String id_pengajuan;
	private String npk;
	private String tgl_cuti;
	private String jml_hari;
	private String tgl_masuk;
	private String keterangan;
	public String getId_pengajuan() {
		return id_pengajuan;
	}
	public void setId_pengajuan(String id_pengajuan) {
		this.id_pengajuan = id_pengajuan;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getTgl_cuti() {
		return tgl_cuti;
	}
	public void setTgl_cuti(String tgl_cuti) {
		this.tgl_cuti = tgl_cuti;
	}
	public String getJml_hari() {
		return jml_hari;
	}
	public void setJml_hari(String jml_hari) {
		this.jml_hari = jml_hari;
	}
	public String getTgl_masuk() {
		return tgl_masuk;
	}
	public void setTgl_masuk(String tgl_masuk) {
		this.tgl_masuk = tgl_masuk;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	@Override
	public String toString() {
		return "ResponseContentModel [id_pengajuan=" + id_pengajuan + ", npk=" + npk + ", tgl_cuti=" + tgl_cuti
				+ ", jml_hari=" + jml_hari + ", tgl_masuk=" + tgl_masuk + ", keterangan=" + keterangan + "]";
	}
}
