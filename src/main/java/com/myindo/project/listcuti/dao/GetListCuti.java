/**
 * 
 */
package com.myindo.project.listcuti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.myindo.project.listcuti.connection.Connect;
import com.myindo.project.listcuti.model.HasilGet;
import com.myindo.project.listcuti.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetListCuti {
	Logger log = Logger.getLogger("GetListCuti");
	String codeResponse = "";
	HasilGet hasilGet ;
	
	public ArrayList<HasilGet> listCuti(RequestModel requestModel) throws Exception {
		ArrayList<HasilGet> hArrayList = new ArrayList<>();
		Connection con = Connect.connection();
		
		try {
			PreparedStatement pp = con.prepareStatement("SELECT * FROM pengajuan_cuti");
			ResultSet rs = pp.executeQuery();
			while (rs.next()) {
				hasilGet = new HasilGet();
				hasilGet.setId_pengajuan(rs.getString("id_pengajuan"));
				hasilGet.setNpk(rs.getString("npk"));
				hasilGet.setTgl_cuti(rs.getString("tgl_cuti"));
				hasilGet.setJml_hari(rs.getString("jml_hari"));
				hasilGet.setTgl_masuk(rs.getString("tgl_masuk"));
				hasilGet.setKeterangan(rs.getString("ket_cuti"));
				
				hArrayList.add(hasilGet);
			}
			rs.close();
			System.out.println("Hasil array : "+hArrayList.toString());
			codeResponse = "0000";
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Gagal");
			log.info(e.getMessage());
			
			hasilGet.setId_pengajuan("");
			hasilGet.setNpk("");
			hasilGet.setTgl_cuti("");
			hasilGet.setJml_hari("");
			hasilGet.setTgl_masuk("");
			hasilGet.setKeterangan("");
		}
		return hArrayList;
	}
}
